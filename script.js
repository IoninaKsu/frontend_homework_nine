"use strict";

//главный объект программы
const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    private: false
};
let numberOfFilms = Nan;

function start() {

    while ((numberOfFilms === '') || (numberOfFilms === null) || (isNaN(numberOfFilms))){
        numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?', '');
    }
    return numberOfFilms;
}


function rememberMyFilms() {

    let numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?");
    let control = Number(numberOfFilms);

    for (let i = 0; i < control; i++) {
        const a = prompt("Один из последних просмотренных фильмов?");
        const b = prompt("На сколько оцените его?");
        if ((a === null) || (b === null) || (a == '') || (b == '') || (a.length > 50)) {
            i--;
            console.log("Error");
        } else {
            personalMovieDB.movies[a] = b;
            console.log("Ok");
        }
    }
}

function detectPersonalLevel(){
    if (personalMovieDB.count < 10) {
        alert("Просмотрено довольно мало фильмов");
    } else if (personalMovieDB.count <= 30) {
            alert("Вы классический зритель");
    } else if (personalMovieDB.count > 30) {
            alert("Вы киноман");
    } else {
            alert("Произошла ошибка");
    }
}

function showMyDB(){
    if (personalMovieDB.privat == false) {
        console.log(personalMovieDB);
    }
}

function writeYourGenres(){
    for(let i = 1; i <= 3; i++){
        const genres = prompt(`Ваш любимый жанр под номером ${i}`);
        if ((genres!= '') && (genre!==null)){
            personalMovieDB.genres[i - 1] = genres;
        }
    }
}

start();
rememberMyFilms();
detectPersonalLevel();
showMyDB();
writeYourGenres();